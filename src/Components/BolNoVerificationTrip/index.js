import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { withStyles,lighten, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import ArrowDropUpRoundedIcon from '@material-ui/icons/ArrowDropUpRounded';
import { ReactComponent as UnFoldMoreIcon } from '../Icons/unfold_more_18px.svg';
import { ReactComponent as DeleteIconFms } from '../Icons/delete.svg';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

function createData(date, FMSLocation, StoreCode, StoreName, BOLNO,Value,Weight,Volume) {
    return { date, FMSLocation, StoreCode, StoreName, BOLNO ,Value,Weight,Volume};
  }
  
  const rows = [
    createData('10-jun-21', 'Chennai DC', 3490, 'Ambattur-Chennai', 28119873468,'Rs 110,952,57',1200,200),
    createData('10-jun-21', 'Chennai DC', 3410, 'Ambattur-Chennai', 28119873468,'Rs 110,57',140,56),
    createData('10-jun-21', 'Chennai DC', 2910, 'Ambattur-Chennai', 28119873468,'Rs 150,50,57',60,47),
  ];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}
 
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}


const headCells = [
    { id: 'date', numeric: false, disablePadding: false, label: 'Date' },
    { id: 'FMSLocation', numeric: true, disablePadding: false, label: 'FMS Location' },
    { id: 'StoreCode', numeric: true, disablePadding: false, label: 'Store Code' },
    { id: 'StoreName', numeric: true, disablePadding: false, label: 'Store Name' },
    { id: 'BOLNO', numeric: true, disablePadding: false, label: 'BOL NO' },
    { id: 'Value', numeric: true, disablePadding: false, label: 'Value' },
    { id: 'Weight', numeric: true, disablePadding: false, label: 'Weight(kgs)' },
    { id: 'Volume', numeric: true, disablePadding: false, label: 'Volume(cft)' },
    { id: 'actions', numeric: true, disablePadding: false, label: 'Actions' },
  ];
  const useStyleshead = makeStyles((theme) => ({
    fordate:{
      minwidth:'100px',
      fontSize:'14px',
      marginLeft:'-20px',
      marginRight:'-20px',
    }
  }));

function EnhancedTableHead(props) {
const {onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
const classeshead = useStyleshead();
  const createSortHandler = (property) => (event) => {
      console.log('test')
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={'center'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
              IconComponent={ UnFoldMoreIcon }
              className={classeshead.fordate}
            >
              {headCell.label}
              
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(even)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  DeleteIcon:{
    width:'20px',
    height:'20px',
    marginLeft:'9px',
    marginTop:'5px'
  },
  BgDeleteIcon:{
    width:'36px',
    height:'30px',
    backgroundColor:'#e6e6e6',
    borderRadius:'8px'
  },
  formControl: {
    backgroundColor:'#e6e6e6',
    border:'0px',
    width:'140px',
    height:'30px',
    borderRadius:'6px',
    paddingLeft:'5px',
    paddingRight:'5px',
  },
}));

export default function BolNoVerificationTrip() {
  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [age, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      console.log('all selected')
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
    console.log('remove the selected')
  };

  const handleRemove = (key) =>{
    console.log('-----',key);
  };

  const handleClick = (event, name) => {
    console.log(event);
    const selectedIndex = selected.indexOf(name);
    console.log('index value',selectedIndex);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
      console.log('one',newSelected);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      console.log('two',newSelected);
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      console.log('three',newSelected);
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      console.log('four',newSelected);
    }

    setSelected(newSelected);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <StyledTableRow
                      tabIndex={-1}
                      key={row.name}
                      
                    >
                      <TableCell component="th" id={labelId} scope="row">
                        {row.date}
                      </TableCell>
                      <TableCell align="center" >{row.FMSLocation}</TableCell>
                      <TableCell align="center" >{row.StoreCode}</TableCell>
                      <TableCell align="center" >{row.StoreName}</TableCell>
                      <TableCell align="center" >{row.BOLNO}</TableCell>
                      <TableCell align="center" >{row.Value}</TableCell>
                      <TableCell align="center" >{row.Weight}</TableCell>
                      <TableCell align="center">{row.Volume}</TableCell>
                      <TableCell align="center">
                    <select name="department" className={classes.formControl}>
                    <option value="parks">Trip Id</option>
                    <option value="rnd">1</option>
                    <option value="finance">2</option>
                    <option value="rec">3</option>
                    <option value="purchasing">4</option>
                    </select>
                      </TableCell>

                      
                    </StyledTableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
