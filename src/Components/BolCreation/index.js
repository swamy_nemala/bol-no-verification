import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepConnector from '@material-ui/core/StepConnector';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { ReactComponent as BolCreationIconActive } from '../Icons/TripCreationProcess/to_bol_active.svg';
import FullWidthTabs from '../BolCreationMenu/index.js';
import SimpleCard from '../BolCreationMenuv1/index.js';
import EnhancedTable from '../PendingBolsTable/index.js';
import CustomizedSteppers1 from '../MenuBar/index.js';
import BolNoVerificationTrip from '../BolNoVerificationTrip/index.js';
import BolNoVerification from '../BolNoVerification/index.js';


const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 40,
  },
  active: {
    '& $line': {
        backgroundColor:'grey',
    },
  },
  completed: {
    '& $line': {
        backgroundColor:'#e85810',
    },
  },
  line: {
    height: 1,
    border: 0,
    backgroundColor: 'grey',
    borderRadius: 1,
    marginLeft:'-10px',
    marginRight:'-10px',
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: 'grey',
    zIndex: 1,
    color: '#fff',
    width: 20,
    height: 20,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor:'grey',
  },
  completed: {
    backgroundColor:'#e85810',
  },

});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  
  const { active, completed } = props;



  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    fontSize:'10px',
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  IconSize:{
      zIndex:1,
      width:'30px',
      height:'30px',
      marginLeft:'82px',
      
  },
  body2:{
      fontSize:'10px',
  }
}));

function getSteps() {
  return ['BOL No verification', 'Trip Initiation', 'Trip Creation','eWaybill Part-B','Trip End'];
}

function getIcons(){
    
    return [<BolCreationIconActive />,<BolCreationIconActive/>,<BolCreationIconActive/>,<BolCreationIconActive/>,<BolCreationIconActive/>];
}




function getStepContent(step) {
  switch (step) {
    case 0:
      return <EnhancedTable/>;
    case 1:
      return <BolNoVerificationTrip/>;
    case 2:
      return <SimpleCard/>;
    case 3:
       return <BolNoVerification/>;
    case 4:
       return <EnhancedTable/>;
    default:
      return <CustomizedSteppers1/>;
  }
}

export default function CustomizedSteppers() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(1);
  const steps = getSteps();
  const Icons = getIcons();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
      console.log('test')
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = (index) => {
    setActiveStep(index);
  };

  return (
    <div className={classes.root}>
      <Stepper alternativeLabel activeStep={activeStep} connector={<ColorlibConnector />}>
        {steps.map((label,index) => (
          <Step key={label} disabled={index > 5} onClick={handleBack}>
              <div className={classes.IconSize}>
              {Icons[index]}
              </div>
            <StepLabel StepIconComponent={ColorlibStepIcon} className={classes.body2}>
                    {label}
            </StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset(0)} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
            <div>{getStepContent(activeStep)}</div>
            <div>
              <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
