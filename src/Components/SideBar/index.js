import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { ReactComponent as DashboardIcon } from '../Icons/LeftMenu/dashboard.svg';
import CustomizedSteppers from '../BolCreation/index.js'
const drawerWidth = 200;

const usestylesBase = makeStyles({
    root:{
        minWidth:'35px',
    },
    IconText:{
        fontSize:'5px',
    }
})

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      backgroundColor:'#fa8e0a',
      color:'black',
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  user_welcome:{
    textAlign:'center',
      marginBottom:-1,
      width:'100%',
  },
  dash:{
      width:'30px',
      height:'30px',
      marginRight:'10',
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    height:'100vh',
  },
  MenuItem:{
    minWidth:'5px',
  },
}));

function ResponsiveDrawer(props) {
  const { window } = props;
  const classes = useStyles();
  const classesbase = usestylesBase();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
        <img className={classes.logo} width="100%" src = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMQ-GyqDphOvZvANPy7M98whGDh3LFRzOj4Q&usqp=CAU" alt="more"/>
        <Typography component="div" className={classes.user_welcome}>
      <Box fontWeight={500} fontSize={15} textAlign='left' ml={4}>
        Welcome
      </Box>
      <Box fontWeight="fontWeightBold" mt={-0.2}  fontSize={18}>
        Prathiba Karnik
      </Box>
    </Typography>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        {['Dashboard', 'Trip Planing', 'Trip creation', 'Transportation','Master'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon className={classesbase.root}>{index % 2 === 0 ? <DashboardIcon className={classes.dash}/> : <MailIcon />}</ListItemIcon>
            <ListItemText fontSize={10} primary={text} className={classesbase.IconText}/>
          </ListItem>
        ))}
      </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            FMS
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
        <main className={classes.content}>
        <div className={classes.toolbar} />
        <CustomizedSteppers/>
        </main>
        
    </div>
    
  );
}
export default ResponsiveDrawer;