import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ArrowRightRoundedIcon from '@material-ui/icons/ArrowRightRounded';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },    
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  Selectone:{
      borderColor:'orange',
      borderBottom:'2px solid',
      textAlign:'center',
      minWidth:"120px",
      height:'30px',
      color:'orange',
  },
  Selectone1:{
    borderColor:'orange',
    textAlign:'center',
    minWidth:"120px",
    height:'30px',
},
  cardone:{
      display:'flex',
      flexDirection:'row',
  },
  SelectIcon:{
    marginTop:'-14px',
  },
  finalone:{
      display:'flex',
      flexDirection:'row',
  }
});

export default function SimpleCard() {
    let text = true;
    let text1 = true;
    let text2 = true;
  const classes = useStyles();

  return (
    <Card className={classes.root}>
        <CardContent className={classes.finalone}> 
      <div className={classes.cardone}>

        <div className={text === true ? classes.Selectone : classes.Selectone1 }>
        <Typography className={classes.title}>
        Select Store
        </Typography>
        </div>
        <div className={classes.SelectIcon} style={{color: text === true ? "orange" : "grey"}}>
        <ArrowRightRoundedIcon style={{ fontSize: 50 }}/>
        </div>
      </div>
      <div className={classes.cardone}>

        <div className={text1 === true ? classes.Selectone : classes.Selectone1 } >
        <Typography className={classes.title}>
        Pending BOLs
        </Typography>
        </div>
        <div className={classes.SelectIcon} style={{color: text1 === true ? "orange" : "grey"}}>
        <ArrowRightRoundedIcon style={{ fontSize: 50 }}/>
        </div>
      </div>
      <div className={classes.cardone}>
        <div className={text2 === true ? classes.Selectone : classes.Selectone1 } >
        <Typography className={classes.title}>
        BOL NO verification
        </Typography>
        </div>
      </div>
      </CardContent>
    </Card>
  );
}
